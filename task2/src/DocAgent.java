package SER421.Lab1.server;

import SER421.Lab1.knowledgeElements.*;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringBufferInputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.ws.ProtocolException;
import java.net.MalformedURLException;
import java.io.IOException;

/**
 * A class to provide a layer of abstraction between the client,
 * HttpUrlConnection and the SAX parser
 * 
 * The README contains some links I referenced to help with the set up of the
 * HttpURLConnection
 * 
 * @Author Chadd Ingersoll
 */
public class DocAgent {

	private String urlString;
	private HttpURLConnection httpConnection;
	private StringBuilder fetchResult;

	public DocAgent(String strServer, int intPort, String strFile) {
		urlString = "HTTP://" + strServer + ":" + intPort + "/" + strFile;
		httpConnection = null;
	}
	
	public DocAgent(String xmlDoc) {
		urlString = "";
		fetchResult = new StringBuilder(xmlDoc);
	}

	public int fetch() {
		int returnCode = 0;
		try {
			URL url = new URL(urlString);
			httpConnection = (HttpURLConnection) url.openConnection();
			httpConnection.setRequestMethod("GET");
			httpConnection.connect();

			// Read the document into memory
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					httpConnection.getInputStream()));
			fetchResult = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				fetchResult.append(line + "\n");
			}
		} catch (MalformedURLException e) {
			System.out.println("CRITICAL: constructed URL was not valid");
			fetchResult = null;
		} catch (ProtocolException e) {
			System.out.println("CRITICAL: The HTTP socket encountered an error");
			fetchResult = null;
		} catch (IOException e) {
			System.out.println("CRITICAL: Unable to communicate with server");
			fetchResult = null;
		} finally {
			if (null != httpConnection) {
				httpConnection.disconnect();
			}
		}

		try {
			returnCode = httpConnection.getResponseCode();
		} catch (Exception e) {
			// If something went wrong prior to this method call,
			// there might not be a code to retrieve
			returnCode = 0;
		}

		return returnCode;
	}
	
	public String getFetchResult() {
		String strReturnable = "";
		if (fetchResult != null) {
			return fetchResult.toString();
		}
		return strReturnable;
	}
	public String parse() {
		
		String strReturnable;
		
		if (fetchResult != null) {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			KnowledgeDocumentParser kdParser = new KnowledgeDocumentParser();
			try {
				InputStream xmlInput = new StringBufferInputStream(
						fetchResult.toString());
	
				SAXParser saxParser = factory.newSAXParser();
				saxParser.parse(xmlInput, kdParser);
				strReturnable = kdParser.toString();
	
			} catch (Throwable e) {
				strReturnable = "";
				e.printStackTrace();
			}
		} else {
			strReturnable = "";
		}
		return strReturnable;
	}
	
	public Knowledge[] getKnowledge() {
		
		Knowledge[] kReturnable = null;
		
		if (fetchResult != null) {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			KnowledgeDocumentParser kdParser = new KnowledgeDocumentParser();
			try {
				InputStream xmlInput = new StringBufferInputStream(
						fetchResult.toString());
	
				SAXParser saxParser = factory.newSAXParser();
				saxParser.parse(xmlInput, kdParser);
				kReturnable = kdParser.getKnowledgeObjects();
				
			} catch (Throwable e) {
				kReturnable = null;
				e.printStackTrace();
			}
		}
		
		return kReturnable;
	}	

}
