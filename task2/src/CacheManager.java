package SER421.Lab1.server;

import SER421.Lab1.knowledgeElements.*;

import java.net.*;
import java.io.*;
import java.util.*;

public class CacheManager {
	
	static private HashMap<String,String> rawCache = new HashMap<String, String>();
	static private String srvName;
	static private int srvPort;
	static private int maxSize;

	/**
	 * @param	serverName	Name of server that we will be proxying
	 * @param	serverPort	Port the destination server is listening on
	 * @param	cSize		The maximum number of objects to cache
	 */
	public CacheManager(String serverName, int serverPort, int cSize) {
		rawCache = new HashMap<String, String>();
	}
	
	public static String getDocument(String docName) {
		String strReturnable = docGet(docName);
		int statusCode;
		
		if (strReturnable == null) {
			
			// Fetch doc from server
			DocAgent dAgent = new DocAgent(srvName, srvPort, docName);
			statusCode = dAgent.fetch();
			if (statusCode == 200) {
				docAdd(docName, dAgent.getFetchResult());
				strReturnable = dAgent.parse();
			}
		} else {
			DocAgent dAgent = new DocAgent(strReturnable);
			strReturnable = dAgent.parse();
		}
		return strReturnable;
	}
	
	// cached documents only!!
	public static String getById(String primaryId) {
		StringBuilder strReturnable = new StringBuilder();
		Stack<Knowledge> shortStack = new Stack<Knowledge>();
		Collection<String> tmpCollection = docEverything();
		
		if (tmpCollection != null) {
			for(String strItem : tmpCollection) {
				DocAgent dAgent = new DocAgent(strItem);
				Knowledge[] kFilter = dAgent.getKnowledge();
				
				for (Knowledge kItem : kFilter) {
					if (primaryId.equals(kItem.getAttributeByName("primarySubjectID")) ) {
						shortStack.push(kItem);
					}
				}
			}
			
			// Call toString on matched Knowledge objects
			while (!shortStack.empty()) {
				strReturnable.append(shortStack.pop().toString());
			}
		}
		return strReturnable.toString();
	}
	
	// cached documents only!!
	public static String getByRater(String primaryId) {
		StringBuilder strReturnable = new StringBuilder();
		Stack<Knowledge> shortStack = new Stack<Knowledge>();
		Collection<String> tmpCollection = docEverything();
		
		if (tmpCollection != null) {
			for(String strItem : tmpCollection) {
				DocAgent dAgent = new DocAgent(strItem);
				Knowledge[] kFilter = dAgent.getKnowledge();
				
				for (Knowledge kItem : kFilter) {
					if (kItem.isRatedBy(primaryId)) {
						shortStack.push(kItem);
					}
				}
			}
			
			// Call toString on matched Knowledge objects
			while (!shortStack.empty()) {
				strReturnable.append(shortStack.pop().toString());
			}
		}
		return strReturnable.toString();
	}
	
	public static synchronized void docAdd(String docName, String docData) {
		if (rawCache.containsKey(docName)) {
			rawCache.remove(docName);
		}
		rawCache.put(docName,docData);
		clean();
	}
	
	public static synchronized String docGet(String docName) {
		return rawCache.get(docName);
	}
	
	public static synchronized boolean docExists(String docName) {
		boolean bolReturnable = false;
		if (rawCache.containsKey(docName)) {
			bolReturnable = true;
		}
		return bolReturnable;
	}
	
	public static synchronized Collection<String> docEverything() {
		return rawCache.values();
	}
	
	private static synchronized void clean() {
		int intRemove = rawCache.size() - maxSize;
		while (intRemove > 0) {
			if (!rawCache.isEmpty()) {
				String[] strKeys = (String[]) rawCache.keySet().toArray(new String[rawCache.size()]);			
				rawCache.remove(strKeys[0]);
			}
			intRemove--;
		}

	}
	

	
}
