package SER421.Lab1.server;

import java.net.*;
import java.io.*;
import java.util.*;

/**
 * A socketed server to handle the transfer/caching of HTML files
 * @author Chadd Ingersoll
 */
public class Main {
	public static CacheManager docCache;
	private static List<HttpSocket> socketList;
	
	// Settings from command line
	private static String targetServer;
	private static int targetPort;
	private static int hostPort;
	private static int cacheSize;
	
	public static void main(String args[]) {
		
		boolean parseError = false;
		try {
			targetServer = args[0];
			targetPort = Integer.parseInt(args[1]);
			hostPort = Integer.parseInt(args[2]);
			cacheSize = Integer.parseInt(args[3]);
			
		} catch (java.lang.ArrayIndexOutOfBoundsException e) {
			System.out.println("CRITICAL: Too few arguments provided");
			parseError = true;
		} catch (java.lang.NumberFormatException e) {
			System.out.println("CRITICAL: There was a problem parsing the arguments");
			parseError = true;
		}
		
		if (!parseError) {
			docCache = new CacheManager(targetServer, targetPort, cacheSize);
			socketList = new ArrayList<HttpSocket>();
		}
		
		while (!parseError) {
			
			// Clean up dead threads
			for (HttpSocket tmpSocket : socketList) {
				if (tmpSocket.getState() == Thread.State.TERMINATED) {
					socketList.remove(tmpSocket);
					tmpSocket = null;
				}				
			}
			
			// Have at least three threads available to do work
			if (socketList.size() < 3) {
				HttpSocket webSock = new HttpSocket(hostPort);
				webSock.run();
				socketList.add(webSock);
			}
		}
	}
	
}
	
	
	
