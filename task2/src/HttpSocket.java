package SER421.Lab1.server;

import java.net.*;
import java.io.*;
import java.util.*;

/**
 * http socket that can respond to simple HTTP requests
 * @see http://docs.oracle.com/javase/7/docs/jre/api/net/httpserver/spec/com/sun/net/httpserver/package-summary.html
 */
public class HttpSocket extends Thread {
	private int listenPort;
	private ServerSocket serverSocket;
	
	public HttpSocket(int inPort) {
		this.listenPort = inPort;
	}
	
	public void run() {
		
		serverSocket = null;
		Socket localSocket = null;
		String strReturnMessage = "HTTP/1.1 200 OK\n\n";
		OutputStream outSock = null;
		InputStream inSock = null;
			
		try {
			serverSocket = new ServerSocket(this.listenPort);
			localSocket = serverSocket.accept();
			outSock = localSocket.getOutputStream();
			inSock = localSocket.getInputStream();
			
			byte clientInput[] = new byte[inSock.available()];
			int buff = inSock.read(clientInput,0,clientInput.length);
			String strRequest = new String(clientInput);
			clientInput = null;
			strRequest.replace("([\\d\\D]+?)(\\QGET\\E)(\\s)(\\S+)(\\s)([\\d\\D]++)","$4");
			strRequest.replace("\\D+//\\D+/","");
			
			if (strRequest.contains("cache/primarySubjectID/")) {
				strRequest.replace("cache/primarySubjectID/","");
				strReturnMessage = strReturnMessage + CacheManager.getById(strRequest);
			} else if (strRequest.contains("cache/Rater/")) {
				strRequest.replace("cache/Rater/","");
				strReturnMessage = strReturnMessage + CacheManager.getByRater(strRequest);
			} else {
				strReturnMessage = strReturnMessage + CacheManager.getDocument(strRequest);
			}
			
			outSock.write(strReturnMessage.getBytes());
			
		} catch (IOException e) {
			//TODO: Handle first world problems
		} finally {
			try {
				if (outSock != null) outSock.close();
				if (inSock != null) inSock.close();
				if (localSocket != null) localSocket.close();
				if (serverSocket != null) serverSocket.close();
			} catch (Exception e) {
				//TODO: Solve first world problems
			}
		}
		
		// Clean up before terminating
		serverSocket = null;
		localSocket = null;
		strReturnMessage = null;
	}
	
}
