package SER421.Lab1.knowledgeElements;

import java.util.Stack;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * The parser target that builds an object out of XML This page provided lots of
 * help; http://tutorials.jenkov.com/java-xml/sax-example.html
 * http://docs.oracle.com/javase/7/docs/api/org/xml/sax/helpers/DefaultHandler.html
 * http://docs.oracle.com/javase/7/docs/api/java/io/StringBufferInputStream.html
 */
public class KnowledgeDocumentParser extends DefaultHandler {

	// Track depth in XML tree
	private Stack<String> elementStack = new Stack<String>();

	// Collapsing stack of XML objects in memory
	private Stack<Knowledge> objectStack = new Stack<Knowledge>();

	private StringBuilder htmlString;
	private String strLastElementType;

	public KnowledgeDocumentParser() {

		htmlString = new StringBuilder();
		strLastElementType = "";

	}

	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {

		// Pop the qualified name to the element stack
		// this will allow us to know the last encountered element
		// a.k.a. breadcrumbs
		elementStack.push(qName);

		// Programatically create a new object
		// TODO: Properly handle the root object; KnowledgeDocument
		Knowledge tmpKnowledge = null;
		if ("KnowledgeAsset".equals(qName)) {
			tmpKnowledge = new KnowledgeAsset(attributes);
		} else if ("KnowledgeRating".equals(qName)) {
			tmpKnowledge = new KnowledgeRating(attributes);
		} else if ("KnowledgeSubject".equals(qName)) {
			tmpKnowledge = new KnowledgeSubject(attributes);
		}

		// If the qualified name is anything else, we will sit tight
		// and merge it into its parent object when we hit the
		// corresponding closing tag (hopefully)

		if (tmpKnowledge != null)
			objectStack.push(tmpKnowledge);
	}

	public void endElement(String uri, String localName, String qName)
			throws SAXException {

		// Tear down breadcrumbs
		elementStack.pop();

		// Merge objects into parent objects
		Knowledge tmpKnowledge;
		if ("KnowledgeRating".equals(qName)) {
			tmpKnowledge = objectStack.pop();
			objectStack.peek().addKnowledgeObject(tmpKnowledge);
		} else if ("KnowledgeSubject".equals(qName)) {

			// We have to be careful with KnowledgeSubject because
			// they could possible be in the root, or nested
			tmpKnowledge = objectStack.pop();
			if (objectStack.peek() instanceof KnowledgeAsset) {
				objectStack.peek().addKnowledgeObject(tmpKnowledge);
			}
		}

	}

	public void characters(char ch[], int start, int length)
			throws SAXException {

		String lastElement = elementStack.peek();
		String value = new String(ch, start, length).trim();

		if (value.length() != 0) {
			if ("title".equals(lastElement)) {
				objectStack.peek().title = value;
			} else if ("description".equals(lastElement)) {
				objectStack.peek().description = value;
			}
		}
	}

	public void endDocument() {
		htmlString.insert(0, "<html><body>");

		for (Knowledge knowledge : objectStack) {
			htmlString.append(knowledge.toString());
		}
		htmlString.append("</body></html>");
	}

	public void fatalError(SAXParseException e) {

	}
	
	public Knowledge[] getKnowledgeObjects() {
		return (Knowledge[]) objectStack.toArray(new Knowledge[objectStack.size()]);
	}

	public String toString() {
		return htmlString.toString();
	}
}
