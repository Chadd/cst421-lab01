package SER421.Lab1.knowledgeElements;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;

public class KnowledgeAsset extends Knowledge {

	// This is here becase the validation says we can expect up to 1
	// KnowledgeSubject in this object
	private Knowledge kSubject;
	private List<Knowledge> ratingList;

	public KnowledgeAsset(Attributes attr) {
		super(attr);
	}

	public String toString() {
		StringBuilder strReturn = new StringBuilder();

		// print the nested subject if it exists
		if (kSubject != null) {
			strReturn.append(kSubject.toString());
		}

		strReturn.append("<h2>" + title + "</h2>\n");
		strReturn.append("<hr />\n");

		// Print date and author information
		strReturn.append("<p>");
		if (localAttributes.containsKey("createdBy")) {
			strReturn.append("Written by: ");
			strReturn.append(localAttributes.get("createdBy"));
			strReturn.append("  ");

			if (localAttributes.containsKey("createdOn")) {
				strReturn.append("and published on: ");
				strReturn.append(localAttributes.get("createdOn"));
			}
		} else if (localAttributes.containsKey("createdOn")) {
			strReturn.append("Published on: ");
			strReturn.append(localAttributes.get("createdOn"));
		}
		strReturn.append("</p>");

		// Print description
		strReturn.append("<p>");
		strReturn.append(description);
		strReturn.append("</p>");

		// Add ratings
		strReturn.append("<ul>");
		for (int i = 0; i < ratingList.size(); i++) {
			strReturn.append("<li>");
			strReturn.append(ratingList.get(i).toString());
			strReturn.append("</li>");
		}
		strReturn.append("</ul>");

		return strReturn.toString();

	}
	
	public boolean isRatedBy(String rater) {
		boolean bolReturnable = false;
		
		for (Knowledge kItem : ratingList) {
			if (rater.equals(kItem.getAttributeByName("Rater"))) {
				bolReturnable = true;
			}
		}
		return bolReturnable;
	}

	void addKnowledgeObject(Knowledge obj) {
		if (obj instanceof KnowledgeSubject) {
			kSubject = obj;
		} else if (obj instanceof KnowledgeRating) {
			if (ratingList == null)
				ratingList = new ArrayList<Knowledge>();
			ratingList.add(obj);
		}
	}

}
