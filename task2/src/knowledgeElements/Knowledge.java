package SER421.Lab1.knowledgeElements;

import java.util.HashMap;
import org.xml.sax.Attributes;

public abstract class Knowledge {

	protected HashMap<String, String> localAttributes;
	public String title;
	public String description;

	public Knowledge() {
		localAttributes = null;
	}

	public Knowledge(Attributes attr) {

		localAttributes = new HashMap<String, String>();
		for (int i = 0; i < attr.getLength(); i++) {
			localAttributes.put(attr.getQName(i), attr.getValue(i));
		}
	}

	public String getAttributeByName(String qName) {
		String strReturn = "";
		if (localAttributes.containsKey(qName)) {
			strReturn = localAttributes.get(qName);
		}
		return strReturn;
	}
	
	public boolean isRatedBy(String rater) {
		
		// Most objects don't have ratings
		return false;
	}

	public String toString() {
		return "";
	}

	abstract void addKnowledgeObject(Knowledge oAdd);
}
