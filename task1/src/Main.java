package SER421.Lab1.Client;

/**
 * XML transfor client
 * @author Chadd Ingersoll <chadd.ingersoll@asu.edu>
 */
public class Main {

	public static void main(String[] argv) {

		String strServer = "";
		int intPort = 0;
		String strFile = "";
		boolean bolValidationFailure = false;

		// Parse command line arguments and report back if problems
		try {
			strServer = argv[0];
			intPort = Integer.parseInt(argv[1]);
			strFile = argv[2];
		} catch (java.lang.ArrayIndexOutOfBoundsException e) {
			System.out.println("CRITICAL: Too few arguments specified");
			System.out.println("          Please use the format of;");
			System.out.println("          <Server name or IP> <port number> <file name>");
			bolValidationFailure = true;
		} catch (java.lang.NumberFormatException e) {
			System.out.println("CRITICAL: An invalid port has been specified");
			System.out.println("          Please use a number between 1 and 65534");
			bolValidationFailure = true;
		}

		// If things havent blown up already, perform further validation
		if (!bolValidationFailure) {
			if (strServer.length() < 1) {
				bolValidationFailure = true;
			}
			if (intPort == 0 || intPort > 65534) {
				bolValidationFailure = true;
				System.out.println("CRITICAL: An invalid port has been specified");
				System.out.println("          Please use a number between 1 and 65534");
			}
			if (strFile.length() < 1) {
				bolValidationFailure = true;
			}
		}

		// If we have gotten this far, we assume the data is clean and proceed
		if (!bolValidationFailure) {
			DocAgent myDocAgent = new DocAgent(strServer, intPort, strFile);
			int intStatusCode = myDocAgent.fetch();

			if (intStatusCode == 200) {

				// Everything looks good, lets parse that data!
				System.out.println(myDocAgent.parse());

			} else {

				// Failure state
				System.out.println("CRITICAL: Error when fetching file: "
						+ intStatusCode);
			}
		}
	}
}
