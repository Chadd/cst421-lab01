package SER421.Lab1.knowledgeElements;

import org.xml.sax.Attributes;

public class KnowledgeRating extends Knowledge {

	public KnowledgeRating(Attributes attr) {
		super(attr);
	}

	public String toString() {
		StringBuilder strReturn = new StringBuilder();

		// Start with the rating
		if (this.localAttributes.containsKey("Rating")) {
			strReturn.append(this.localAttributes.get("Rating"));
			strReturn.append(" out of 10 ");
			if (localAttributes.containsKey("Rater")) {
				strReturn.append("from user: ");
				strReturn.append(this.localAttributes.get("Rater"));
				strReturn.append(" ");
			}
			if (description != null) {
				strReturn.append("with the comment: ");
				strReturn.append(description);
			}
		}

		return strReturn.toString();
	}

	void addKnowledgeObject(Knowledge oAdd) {
		// This type has no sub-objects, so we will be lazy and
		// just ignore any calls to this method
	}
}
