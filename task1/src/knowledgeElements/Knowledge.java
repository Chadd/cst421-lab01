package SER421.Lab1.knowledgeElements;

import java.util.HashMap;

import org.xml.sax.Attributes;

public abstract class Knowledge {

	protected HashMap<String, String> localAttributes;
	public String title;
	public String description;

	public Knowledge() {
		localAttributes = null;
	}

	public Knowledge(Attributes attr) {

		localAttributes = new HashMap<String, String>();
		for (int i = 0; i < attr.getLength(); i++) {
			localAttributes.put(attr.getQName(i), attr.getValue(i));
		}
	}

	public String getAttributeByName(String qName) {
		String strReturn = "";
		if (localAttributes.containsKey(qName)) {
			strReturn = localAttributes.get(qName);
		} else {
			strReturn = "";
		}
		return strReturn;
	}

	public String toString() {
		return "";
	}

	abstract void addKnowledgeObject(Knowledge oAdd);
}
