package SER421.Lab1.knowledgeElements;

import org.xml.sax.Attributes;

public class KnowledgeSubject extends Knowledge {

	public KnowledgeSubject(Attributes attr) {
		super(attr);
	}

	public String toString() {
		StringBuilder strReturn = new StringBuilder();
		strReturn.append("<h1><u>");
		if (title != null) {
			strReturn.append(title);
		}
		if (description != null) {
			strReturn.append(" - ");
			strReturn.append(description);
		}
		strReturn.append("</u></h1>");
		return strReturn.toString();
	}

	void addKnowledgeObject(Knowledge oAdd) {
		// This object does not have any child objects according
		// to the validation schema. So we are ignoring calls here
	}
}
